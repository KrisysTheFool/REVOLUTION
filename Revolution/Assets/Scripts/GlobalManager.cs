﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Cette classe sert à gérer le démarrage de parties, ainsi que le redémarrage de parties
public class GlobalManager : MonoBehaviour
{
    public static GlobalManager instance;

    // Cette répartition sert uniquement lors de la première exécution. Au-delà de ça, les rôles sont répartis par le PlayerRoleManager
    [SerializeField]
    PlayerRoleManager.Roles[] playerRoles = new PlayerRoleManager.Roles[4];

    [SerializeField]
    GameObject[] playerPrefabs;

    [SerializeField]
    GameObject bossPrefab;

    [SerializeField]
    Vector2 size;

    public int lastWinningPlayer;

    void Start()
    {
        if(instance)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    // Méthode à appeler pour commencer la première run d'une session, spécifier si lancé depuis le launcher ou indépendamment
    // Si lancement depuis launcher, c'est le launcher qui définit les rôles, sinon c'est le tableau présent plus haut
    public void StartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("_Main");
	}
    // Cette fonction est à appeler en cas de redémarrage de partie
    public void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("_Main");
        PlayerRoleManager.instance.RelaunchRoles(lastWinningPlayer);
    }

    // Cette fonction utilitaire sert à générer les personnages des révolutionnaires sur la carte

    public void GenerateCharacters()
    {
        int numberOfInstanciated = 0;
        int nextPlayerToLookAt = 0;
        int numberOfIndividuals = PlayerRoleManager.instance.playerNumber - 1;

        if(playerPrefabs.Length > 0)
        {
            while (numberOfInstanciated < numberOfIndividuals)
            {
                float x = Random.Range(transform.position.x - size.x / 2f, transform.position.x + size.x / 2f);
                float y = Random.Range(transform.position.y - size.y / 2f, transform.position.y + size.y / 2f);

                int prefabIndex = Random.Range(0, playerPrefabs.Length);

                Collider playerPrefabCollider = playerPrefabs[prefabIndex].GetComponent<Collider>();
                if (playerPrefabCollider)
                {
                    if (!Physics.CheckBox(new Vector3(x, 0.5f, y), playerPrefabCollider.bounds.extents))
                    {
                        GameObject newPlayer = Instantiate(playerPrefabs[prefabIndex], new Vector3(x, 0.9f, y), Quaternion.identity);
                        InputController newPlayerController = newPlayer.GetComponent<InputController>();
                        if (newPlayerController)
                        {
                            while (nextPlayerToLookAt < 4 && PlayerRoleManager.instance.GetPlayerRole(nextPlayerToLookAt) != PlayerRoleManager.Roles.Rebel)
                            {
                                nextPlayerToLookAt++;
                            }
                            if (nextPlayerToLookAt >= 4)
                            {
                                throw new System.Exception("Trying to create more communists than the number of communist players");
                            }
                            else
                            {
                                newPlayerController.playerId = nextPlayerToLookAt;
                                nextPlayerToLookAt++;
                            }
                        }
                        numberOfInstanciated++;
                    }
                }
                else
                {
                    throw new System.Exception("No collider on player prefab !");
                }
            }

        }
    }

    public void GenerateBoss()
    {
        int capitalistId = -1;
        for (int i = 0; i < 4; i++)
        {
            if(PlayerRoleManager.instance.GetPlayerRole(i) == PlayerRoleManager.Roles.Capitalist)
            {
                capitalistId = i;
            }
        }
        GameObject boss = Instantiate(bossPrefab);
        if(boss.GetComponent<BossController>())
        {
            boss.GetComponent<BossController>().playerId = capitalistId;
        }

    }
}
