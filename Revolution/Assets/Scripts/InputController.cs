﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : Controller
{
    public int playerId;
    public float sprintSpeed = 1.5f;
    public bool isSprinting= false;
	
	// Update is called once per frame
	void Update()
    {
        direction.x = Input.GetAxisRaw("Horizontal"+playerId.ToString());
        direction.z = Input.GetAxisRaw("Vertical"+ playerId.ToString());
        if(Input.GetButtonDown("A"+playerId.ToString()))
        {
            this.gameObject.GetComponent<Commoner>().Manif();
        }
		if (Input.GetButtonDown ("Y" + playerId.ToString ())) {
			this.gameObject.GetComponent<Commoner> ().Highlight ();
		}
		if (Input.GetButtonUp ("Y" + playerId.ToString ())) {
			this.gameObject.GetComponent<Commoner> ().EndLight ();
		}		 if(Input.GetButtonDown("B"+playerId.ToString()))
        {

            isSprinting = true;
        }

        if(Input.GetButtonUp("B" + playerId.ToString()))

        {

            isSprinting = false;
        }	}

}
