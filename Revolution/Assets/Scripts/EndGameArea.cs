﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameArea : MonoBehaviour
{
	public int timeUp;
	float timeUpf;
	float timeMax;

	void Start(){
		timeUpf = timeUp;
		timeMax = Time.time + timeUp;
	}

	void Update(){
		timeUpf -= Time.deltaTime;
		timeUp = (int)timeUpf;
		if (Time.time>timeMax) {
			GlobalManager.instance.lastWinningPlayer =FindObjectOfType<Boss>().GetComponent<BossController>().playerId;
			UnityEngine.SceneManagement.SceneManager.LoadScene("CapitalistVictoryScreen");
		}
	}

    private void OnTriggerEnter(Collider other)
    {
        InputController winnerController = other.gameObject.GetComponent<InputController>();
        if(winnerController)
        {
            GlobalManager.instance.lastWinningPlayer = winnerController.playerId;
            UnityEngine.SceneManagement.SceneManager.LoadScene("CommunistVictoryScreen");
        }
    }
}
