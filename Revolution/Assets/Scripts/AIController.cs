﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller {

    public bool isInARiotInfluenceRadius;
    public bool isInARiot;
    public bool hasDecidedToJoinARiot;
    public bool hasJustLeftARiot;
    //public Riot riot;
    public GameObject riot;
    public LayerMask layerMask;
    //Renderer rend;

	public Vector2 straightWalkTimeRange;
	private float straightWalkTime{
		get{
			return UnityEngine.Random.Range (straightWalkTimeRange.x, straightWalkTimeRange.y); 
		}
		set{

		}
	}
	private float endStraightWalkTime;
	//public bool showVibrisses;
	//public float distanceToWalls;

	void Start(){
		endStraightWalkTime = Time.time + straightWalkTime;
		direction = new Vector3(UnityEngine.Random.Range(-1f,1f),0,UnityEngine.Random.Range(-1f,1f)).normalized;
        //rend = gameObject.GetComponent<Renderer>();
        //hasDecidedToJoinARiot = false;
        //isInARiot = false;
        //isInARiotInfluenceRadius = false;
	}

    void Update()
    {
        //if (rend == null)
        //{
        //    rend = gameObject.GetComponent<Renderer>();
        //}
        if (riot == null)
        {
            hasDecidedToJoinARiot = false;
            if (isInARiot)
            {
                hasJustLeftARiot = true;
            }
            else
            {
                hasJustLeftARiot = false;
            }
            isInARiot = false;
            isInARiotInfluenceRadius = false;
        }
        /*
		RaycastHit hit;
		if (Physics.Raycast(transform.position, direction, out hit, 2*distanceToWalls+0.5f, LayerMask.NameToLayer("BuildingsMask"))
			|| Physics.Raycast(transform.position, Vector3.Cross(direction, Vector3.up), out hit, distanceToWalls+0.5f, LayerMask.NameToLayer("BuildingsMask"))
			|| Physics.Raycast(transform.position, Vector3.Cross(Vector3.up, direction), out hit, distanceToWalls+0.5f, LayerMask.NameToLayer("BuildingsMask"))){ //valeur en dure à changer si personnage plus gros que 1
			Debug.Log("hu");
			direction+=hit.normal * (1-((hit.distance-0.5f)/2*distanceToWalls));
			direction = direction.normalized;
		}
		*/
        if (Time.time > endStraightWalkTime)
        {
            UpdateDirection();
        }

        if (isInARiotInfluenceRadius)
        {
            if (isInARiot)
            {
                //Debug.Log("isInARiot");
                if (riot != null)
                {
                    float step = 3f;
                    direction = (riot.transform.position - transform.position).normalized;
                    direction += new Vector3(UnityEngine.Random.Range(-step,step), 0f, UnityEngine.Random.Range(-step, step));
                    direction.y = 0;
                }
                //rend.material.SetColor("_Color", Color.green);
            }
            else
            {
                //rend.material.SetColor("_Color", Color.blue);
            }
            if (hasJustLeftARiot)
            {
                UpdateDirection();
            }
        }
        else
        {
            
            //rend.material.SetColor("_Color", Color.red);
        }
    }
    void OnCollisionEnter(Collision collision){
		endStraightWalkTime += straightWalkTime;
		foreach (ContactPoint contact in collision.contacts){
			float rand = UnityEngine.Random.Range (0f, 1f);
			direction = rand*contact.normal + UnityEngine.Random.Range (-1f, 1f)*(1-rand)*Vector3.Cross(Vector3.up, contact.normal);
		}
		direction = direction.normalized;
	}

	void UpdateDirection(){
        if (!isInARiot || hasJustLeftARiot)
        {
            endStraightWalkTime += straightWalkTime;
            direction = new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f)).normalized;
        }
     }
    /*
	void OnDrawGizmos(){
		if (showVibrisses) {
			Gizmos.color = Color.black;
			Gizmos.DrawLine (transform.position, transform.position + direction *  (2*distanceToWalls+0.5f));
			Gizmos.DrawLine (transform.position, transform.position +  Vector3.Cross(direction, Vector3.up) *  (distanceToWalls+0.5f));
			Gizmos.DrawLine (transform.position, transform.position + Vector3.Cross(Vector3.up, direction) *  (distanceToWalls+0.5f));
		}
	}
	*/
    private void OnTriggerStay(Collider other)
    {
        //Riot _riot = other.gameObject.GetComponent<Riot>();
        //if (_riot != null)
        if (other.tag == "Riot") 
        {
            isInARiotInfluenceRadius = true;
            //Debug.Log("Je suis dans le rayon d'influence d'une manif");
            //Vector3 direction = _riot.transform.position - transform.position;
            Vector3 direction = other.transform.position - transform.position;


            if (!Physics.Raycast(transform.position, direction, direction.magnitude, layerMask)
                && !hasDecidedToJoinARiot)
            {
                //Debug.Log("Raycast passé, decideIfjoinRiot");
                //decideIfJoinRiot(_riot);
                decideIfJoinRiot(other.gameObject);

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Riot _riot = other.gameObject.GetComponent<Riot>();
        //if (_riot != null)
        if (other.tag == "Riot")
        {
            //leaveRiot(_riot);
            leaveRiot(other.gameObject);
        }
    }

    private void decideIfJoinRiot(GameObject _riot)
    {
        //Debug.Log("decideIfJoinRiot");
        hasDecidedToJoinARiot = true;
        if (true) // Ici on mettra une condition qui permet de décider aléatoirement si oui ou non on joinRiot
        {
            joinRiot(_riot);
        }
    }

    private void joinRiot(GameObject _riot)
    {
        riot = _riot;
        isInARiot = true;
    }

    private void leaveRiot(GameObject riot)
    {
        //Debug.Log("leaveRiot");
        riot = null;
        isInARiot = false;
        hasDecidedToJoinARiot = false;
        isInARiotInfluenceRadius = false;
    }
}
