﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneLoader : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        GlobalManager.instance.GenerateCharacters();
        GlobalManager.instance.GenerateBoss();
	}
}
