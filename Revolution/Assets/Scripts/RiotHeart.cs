﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiotHeart : MonoBehaviour {
    public Riot parentRiot;

    private void Start()
    {
        parentRiot = transform.parent.GetComponent<Riot>();
    }
    public void DestroyRiot(){
		Destroy (this.gameObject);
		Destroy (transform.parent.gameObject);
		Riot.riotList.Remove (transform.parent.GetComponent<Riot>());
	}

    private void OnTriggerStay(Collider other)
    {
        //AIController otherAIController = other.gameObject.GetComponent<AIController>();
        //if (otherAIController != null)
        int temp = 0;
        if (other.tag == "AICharacter")
        {
            //Debug.Log("otherAIController entered HeartRiot trigger");
             temp += 1;
             parentRiot.nbOfRioters = temp;
        }
    }

}
