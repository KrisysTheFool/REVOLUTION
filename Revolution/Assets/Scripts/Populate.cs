﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Populate : MonoBehaviour {

	public GameObject[] individualPrefabs;
	public Vector2 size;
	public int numberOfIndividuals;

	// Use this for initialization
	void Start () {
        if(individualPrefabs.Length > 0)
        {
            int numberOfInstanciated = 0;
            while (numberOfInstanciated < numberOfIndividuals)
            {
                float x = Random.Range(transform.position.x - size.x / 2f, transform.position.x + size.x / 2f);
                float y = Random.Range(transform.position.y - size.y / 2f, transform.position.y + size.y / 2f);
                int i = Random.Range(0, individualPrefabs.Length);
                Collider individualCollider = individualPrefabs[i].GetComponent<Collider>();
                if (individualCollider)
                {
                    if (!Physics.CheckBox(new Vector3(x, 0.5f, y), individualCollider.bounds.extents))
                    {
						Instantiate(individualPrefabs[i], new Vector3(x, 0.8f, y), Quaternion.identity);
                        numberOfInstanciated++;
                    }
                }
                else
                {
                    throw new System.Exception("No collider on individual");
                }
            }

        }
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.white;
		Gizmos.DrawWireCube(transform.position, new Vector3(size.x, 10, size.y));
	}
}
