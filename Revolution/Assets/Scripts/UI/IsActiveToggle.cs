﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsActiveToggle : MonoBehaviour
{
    UnityEngine.UI.Toggle isActive;

    [SerializeField]
    int playerId;

    // Use this for initialization
    void Start()
    {
        isActive = GetComponent<UnityEngine.UI.Toggle>();
    }

    public void TransmitStateChangeData()
    {
        if (!isActive.isOn)
        {
            PlayerRoleManager.instance.AffectRole(playerId, PlayerRoleManager.Roles.NotAffected);
        }
        else
        {
            PlayerRoleManager.instance.AffectRole(playerId, PlayerRoleManager.Roles.Rebel);
        }
    }

    private void Update()
    {
        isActive.isOn = !(PlayerRoleManager.instance.GetPlayerRole(playerId) == PlayerRoleManager.Roles.NotAffected);
    }
}
