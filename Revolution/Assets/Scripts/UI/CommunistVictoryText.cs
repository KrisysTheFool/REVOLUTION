﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommunistVictoryText : MonoBehaviour
{
    UnityEngine.UI.Text text;

	// Use this for initialization
	void Start ()
    {
        text = GetComponent<UnityEngine.UI.Text>();
        text.text = "Player " + (GlobalManager.instance.lastWinningPlayer +1) + " succeeds to overthrow the tyrant... However, human nature being what it is, he quickly realizes that the best way of helping people would be to control their lives.";
	}

    void Update()
    {
        if(Input.anyKeyDown)
        {
            GlobalManager.instance.RestartGame();
        }
    }
}
