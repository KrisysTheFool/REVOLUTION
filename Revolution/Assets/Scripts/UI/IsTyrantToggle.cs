﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTyrantToggle : MonoBehaviour
{
    UnityEngine.UI.Toggle isTyrant;

    [SerializeField]
    int playerId;

	// Use this for initialization
	void Start ()
    {
        isTyrant = GetComponent<UnityEngine.UI.Toggle>();
	}
	
	public void TransmitStateChangeData()
    {
        if(isTyrant.isOn)
        {
            PlayerRoleManager.instance.AffectRole(playerId, PlayerRoleManager.Roles.Capitalist);
        }
        else
        {
            PlayerRoleManager.instance.AffectRole(playerId, PlayerRoleManager.Roles.Rebel);
        }
    }

    private void Update()
    {
        if(PlayerRoleManager.instance.GetPlayerRole(playerId) != PlayerRoleManager.Roles.Capitalist)
        {
            isTyrant.isOn = false;
            isTyrant.interactable = (PlayerRoleManager.instance.GetPlayerRole(playerId) != PlayerRoleManager.Roles.NotAffected);
        }
        else
        {
            isTyrant.isOn = true;
        }
    }
}
