﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapitalistVictoryText : MonoBehaviour
{
    UnityEngine.UI.Text text;

	// Use this for initialization
	void Start ()
    {
        text = GetComponent<UnityEngine.UI.Text>();
        text.text = "Player " + GlobalManager.instance.lastWinningPlayer + " has once again given an example of application of the third law of capitalism : any amount of effort or will can be countered by an equivalent amount of money and minions.";
	}

    void Update()
    {
        if(Input.anyKeyDown)
        {
            GlobalManager.instance.RestartGame();
        }
    }
}
