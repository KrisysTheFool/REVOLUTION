﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButton : MonoBehaviour
{
    UnityEngine.UI.Button button;

	// Use this for initialization
	void Start ()
    {
        button = GetComponent<UnityEngine.UI.Button>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        bool tyrantFound = false;
		for (int i = 0; i < 4; i++)
        {
            if(PlayerRoleManager.instance.GetPlayerRole(i) == PlayerRoleManager.Roles.Capitalist)
            {
                tyrantFound = true;
            }
        }
        if(tyrantFound && PlayerRoleManager.instance.playerNumber > 1)
        {
            button.interactable = true;
        }
        else
        {
            button.interactable = false;
        }
	}

    public void Launch()
    {
        GlobalManager.instance.StartGame();
    }
}
