﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiControllerHandler : MonoBehaviour {

    public static MultiControllerHandler Instance;

    [SerializeField]private int numberOfControllers = 0;
    [SerializeField]private InputController[] inputControllers; //à initialiser correctement dans l'éditeur
    private PlayerRoleManager roleManager; //à initialiser correctement dans l'éditeur
    public int CapitalistIndex = -1;
    float timer = 1.0f;
    [SerializeField] float controllerUpdateTimer = 2.0f;

    private void Awake()
    {
        if(Instance!=null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    // Use this for initialization
    void Start ()
    {
        DontDestroyOnLoad(this);
        numberOfControllers = Input.GetJoystickNames().Length;
        roleManager = PlayerRoleManager.instance;
        CheckControllers();
        timer = 0.0f;
    }

	
	// Update is called once per frame
	void Update ()
    {
        numberOfControllers = Input.GetJoystickNames().Length;
        timer += Time.deltaTime;
        if(timer>controllerUpdateTimer)
        {
            CheckControllers();
            timer = 0.0f;
        }
    }

    private void CheckControllers()
    {
        string[] temp = Input.GetJoystickNames();

        if(temp.Length >0)
        {
            for (int i = 0; i < temp.Length;i++)
            {
                //Check if the string is empty or not
                if (!string.IsNullOrEmpty(temp[i]))
                {
                    //Not empty, controller temp[i] is connected
                    
                    /*if (i < 4)
                    {
                        if(roleManager.GetPlayerRole(i)==PlayerRoleManager.Roles.NotAffected)
                        {
                            if (i != CapitalistIndex)
                                roleManager.AffectRole(i, PlayerRoleManager.Roles.Rebel);
                            else
                                roleManager.AffectRole(i, PlayerRoleManager.Roles.Capitalist);
                        }
                    }*/
                    
                }
                else
                {
                    //If it is empty, controller i is disconnected
                    //where i indicates the controller number
                    //Debug.Log("Controller: " + i + " is disconnected.");
                    /*if (i < 4)
                    {
                        roleManager.AffectRole(i, PlayerRoleManager.Roles.NotAffected);
                    }*/

                }
            }
        }
    }

    private void AddInputControllers(GameObject player,int index)
    {
        if (player.GetComponent<InputController>())
            inputControllers[index] = player.GetComponent<InputController>();
        else
            throw new UnityException("InputController Component doesn't exist");
    }
}
