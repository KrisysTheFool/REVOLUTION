﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Riot : MonoBehaviour {

    public static List<Riot> riotList = new List<Riot>();

    public Vector3 position;
    public GameObject riotHeart ;// inutilisé pour le moment
    public float influenceRadius; // Rayon d'influence de la manifestation
    public float influenceRadiusMinValue; // Rayon d'influence minimum
    public float influenceRadiusGrowthRate;
    public float influenceRadiusGrowthRatePerRioter; // Combien on rajoute en rayon par Rioter

    public int nbOfRioters;


    private void Awake()
    {

        if (riotList == null)
        {
            riotList = new List<Riot>();
        }

        riotList.Add(this);
    }

    private void OnDestroy()
    {
        riotList.Remove(this);

    }
    // Use this for initialization
    void Start () {
        Boss boss = FindObjectOfType<Boss>();
        if (boss != null)
        {
            boss.SeekRiot(gameObject.transform.position);
        }

        riotHeart = transform.GetChild(0).gameObject;

        if (influenceRadius == 0)
        {
            influenceRadius = influenceRadiusMinValue;
        }
        if (influenceRadiusGrowthRate == 0)
        {
            influenceRadiusGrowthRate = influenceRadiusMinValue;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (riotHeart == null)
        {
            riotHeart = transform.GetChild(0).gameObject;
        }
        influenceRadiusGrowthRate = influenceRadiusGrowthRatePerRioter * nbOfRioters;
        influenceRadius = influenceRadiusMinValue + influenceRadiusGrowthRate;
        transform.localScale = new Vector3(influenceRadius, 0.25f, influenceRadius);
        riotHeart.transform.localScale = new Vector3(1f/3f, 1.2f, 1f/3f);
        //transform.position = position;
	}

    private void OnTriggerEnter(Collider other)
    {
        AIController otherAIController = other.gameObject.GetComponent<AIController>();
        if (otherAIController != null)
        {
            //Debug.Log("otherAIController entered Riot trigger");
            nbOfRioters += 1;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        AIController otherAIController = other.gameObject.GetComponent<AIController>();
        if (otherAIController != null)
        {
            //Debug.Log("otherAIController exited Riot trigger");
            nbOfRioters -= 1;
        }
    }
}
