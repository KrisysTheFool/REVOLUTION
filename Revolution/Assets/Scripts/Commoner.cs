﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Commoner : Character
{
    [SerializeField]private bool stunned =false;
    private float previousSpeed = 0.0f;
    [SerializeField]private float timer = 0.0f;
    [SerializeField] float stunDuration = 1.0f;
	public float speedRandomizer = 0;
    [SerializeField] private GameObject riotPrefab;
	public bool highlight;
    [SerializeField] private AudioClip paperDrop;
    [SerializeField] private AudioClip playerHit01;
    [SerializeField] private AudioClip playerPunch01;
    [SerializeField] AudioSource audioSource;
    // Use this for initialization
    override protected void Start ()
    {
		base.Start ();
		speed += UnityEngine.Random.Range (-speedRandomizer, speedRandomizer);
        controller = GetComponent<Controller>();
     

    }

    protected override void Update()
    {
        base.Update();
        if (stunned)
        {
            timer += Time.deltaTime;
            if (timer > stunDuration)
            {
                speed = previousSpeed;
                stunned = false;
            }
        }
    }

    protected override void SetDirection()
    {
      
        direction = controller.direction;
    }

    public void Stun()
    {
        if (!stunned)
        {
            audioSource.PlayOneShot(playerPunch01, 1F);
            audioSource.PlayOneShot(playerHit01, 0.8F);
            previousSpeed = speed;
            timer = 0.0f;
            speed = 0.0f;
            stunned = true;
        }
    }

    public void Manif()
    {
        audioSource.PlayOneShot(paperDrop, 1F);
        GameObject riot = Instantiate(riotPrefab);
        riot.transform.position = this.transform.position;
    }

	public void Highlight(){
		highlight = true;
		transform.GetComponentInChildren<Renderer> ().material.SetColor ("_Color", Color.red);
	}
	public void EndLight(){
		highlight = false;
		transform.GetComponentInChildren<Renderer> ().material.SetColor ("_Color", Color.white);
	}
}
