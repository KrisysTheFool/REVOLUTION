﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {

    public static Boss instance;

	List<Cop> cops = new List<Cop>();
	[SerializeField] int activeCopIndex;
	public Vector3 direction;

    private void Awake()
    {
        if(instance)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    // Use this for initialization
    void Start () {
		//GameObject bossFreeHandMode = new GameObject ();
		//bossFreeHandMode.name = "BossFreeHandMode";
		//bossFreeHandMode.AddComponent<BossFreeHandMode> ();
		//cops.Add((bossFreeHandMode.GetComponent<Cop>()));	//Regarder la classe ou demander à Simon si il ne dort pas
	}

	void Update(){
		cops [activeCopIndex].SetActiveCop();
		//if (cops.Count < 1) {
		//	cops.Add(new BossFreeHandMode());	
		//}

	}

	public int AddCop(Cop cop){ //Les cops gèrent leur addition à la liste à leur création comme des grands
		cops.Add (cop);
		return cops.Count-1;
	}

	public void RemoveCop(int index){ // Les cops gèrent leur destruction eux-mêmes
		if (index == activeCopIndex) {
			IncrementActiveCopIndex ();
		}
		cops.RemoveAt (index);
		for (int i = index; i < cops.Count; i++) {
			cops [i].ReduceIndex();
		}
	}

	public void IncrementActiveCopIndex(){
		cops [activeCopIndex].SetInactiveCop();
		activeCopIndex++;
		if (activeCopIndex >= cops.Count) {
			activeCopIndex = 0;
		}
		cops [activeCopIndex].SetActiveCop();
	}

	public void DecrementActiveCopIndex(){
		cops [activeCopIndex].SetInactiveCop();
		activeCopIndex--;
		if (activeCopIndex < 0) {
			activeCopIndex = cops.Count-1;
		}
		cops [activeCopIndex].SetActiveCop();
	}

	public void SetActiveCopIndex(int i){
		cops [activeCopIndex].SetInactiveCop();
		activeCopIndex = i;
		cops [activeCopIndex].SetActiveCop();
	}

	public void SeekRiot(Vector3 riotPos){
		bool copFound = false;
		for (int i = 0; i < cops.Count; i++) {
			Debug.Log (i + "th cop is active : " + cops [i].isActiveCop);
			Debug.Log (i + "th cop is busy : " + cops [i].isBusy);
			if (!cops [i].isActiveCop && !cops [i].isBusy) {
				copFound = true;
				cops [i].target = riotPos;
			}
			if (copFound) {
				Debug.Log (i + "th cop is chosen");
				break;
			}
		}
		/*
float distance = float.MaxValue;
		int indexcop = -1;
		for (int i = 0; i < cops.Count; i++) {
			if (!cops [i].isActiveCop) {
				if (distance > (transform.position - riotPos).sqrMagnitude) {
					distance = (transform.position - riotPos).sqrMagnitude;
					indexcop = i;
				}
			}
		}
		if (indexcop > -1) {
			cops [indexcop].target = riotPos;
		}
		*/
	}
}
