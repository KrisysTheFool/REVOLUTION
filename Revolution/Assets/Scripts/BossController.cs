﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour {

    public static BossController instance;


    public int playerId;
    private Vector3 copVelocity = Vector3.zero;
    private Boss boss;
    public bool isSprinting;

    private void Awake()
    {
        if(instance!=null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }

    }
    // Use this for initialization
    void Start ()
    {
        boss = Boss.instance;
	}
	
	// Update is called once per frame
	void Update ()
    {
    

        copVelocity = new Vector3(Input.GetAxis("Horizontal" + playerId.ToString()), 0.0f, Input.GetAxis("Vertical" + playerId.ToString()));
        boss.direction = copVelocity;
        if(Input.GetButtonDown("GachetteDroite" + playerId.ToString()))
        {
            boss.IncrementActiveCopIndex();
        }
        else if(Input.GetButtonDown("GachetteGauche" + playerId.ToString()))
        {
            boss.DecrementActiveCopIndex();
        }
        if ( Input.GetButton("B" + playerId.ToString()))
        {
            isSprinting = true;
        }
        else
        {
            isSprinting = false;
        }

        
	}
}
