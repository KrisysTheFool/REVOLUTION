﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

    public RevolutionController revolutionController;
    public Color initialColor;
    public Color finalColor;
    public Light light;

    // Use this for initialization
    void Start () {
        revolutionController = RevolutionController.instance;
        light = gameObject.GetComponent<Light>();

	}
	
	// Update is called once per frame
	void Update () {

		if (revolutionController == null)
        {
            revolutionController = RevolutionController.instance;
        }

        if (light == null)
        {
            light = gameObject.GetComponent<Light>();
        }

        float revolutionLevel = revolutionController.Level;



        light.color = Color.Lerp(initialColor, finalColor, revolutionLevel);



	}
}
