﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RevolutionController : MonoBehaviour {

    public static RevolutionController instance;

    private int rioters;
    public float Level;
    [SerializeField] private Slider slider;
    [SerializeField] private float decreaseRate = -0.05f;
    [SerializeField] private float riotincreaseRate = 0.01f;
    [SerializeField] private AudioClip gauge01;
    [SerializeField] private AudioClip gauge02;
    [SerializeField] private AudioClip gauge03;
    [SerializeField] AudioSource audioSource;

    private enum State
    {
        nil =0,
        mid =1,
        top =2,
        alert =3
    }
   [SerializeField] private State currentState;
    // Use this for initialization
    private void Awake()
    {
        if(instance)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    void Start ()
    {
        Level = 0;
        Cop.setAlertMode(false);
        slider.value =Level;
    }
	
	// Update is called once per frame
	void Update ()
    {
       
        if (Riot.riotList.Count == 0)
        {
            Level += decreaseRate * Time.deltaTime;
            
        }
        else
        {
            Level += riotincreaseRate * Time.deltaTime;
        }
       
        slider.value = Level;


        switch(currentState)
        {
            case State.nil:
                if (Level >0.5)
                {
                    currentState = State.mid;
                    audioSource.PlayOneShot(gauge01, 0.7F);
                }
            break;
            case State.mid:
                if (Level > 0.75)
                {
                    audioSource.PlayOneShot(gauge02, 0.7F);
                    currentState = State.top;
                }
                else if (Level <= 0.4)
                {
                    currentState = State.nil;
                }
                break;
            case State.top:
                if (Level + (decreaseRate + rioters * riotincreaseRate) * Time.deltaTime > slider.maxValue)
                {
                    audioSource.PlayOneShot(gauge03, 0.7F);
                    currentState = State.alert;
                    Cop.setAlertMode(true);
                }
                else if(Level <=0.6)
                {
                    currentState = State.mid;
                }
            break;
            case State.alert:
            break;


        }
		

	}

    public void IncreaseLevel(float increase)
    {
        Level += increase;
        if(Level > slider.maxValue)
        {
            Level = slider.maxValue;
        }
        if(Level < slider.minValue)
        {
            Level = slider.minValue;
        }
    }

}
