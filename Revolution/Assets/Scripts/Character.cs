﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Character : MonoBehaviour
{
    [SerializeField]
    Animator animator;

	[SerializeField]
    protected float speed;
	[SerializeField]
    protected Vector3 direction;
    protected Rigidbody rigidbody;
    protected Controller controller;

    float upDirection;
    float rightDirection;

    // Use this for initialization
    virtual protected void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Move()
    {
        if(!rigidbody)
        {
            rigidbody = GetComponent<Rigidbody>();
        }
        else
        {
            if (controller is InputController)
            {
                if (((InputController)controller).isSprinting)
                {
                    rigidbody.velocity = direction * speed * ((InputController)controller).sprintSpeed;
                }
                else
                {
                    rigidbody.velocity = direction * speed;
                }
            }
            else
            {
                rigidbody.velocity = direction * speed;
            }
        }
    }

    abstract protected void SetDirection();
	
	// Update is called once per frame
	virtual protected void Update ()
    {
        SetDirection();
        if (animator)
        {
            upDirection = direction.z;
            rightDirection = direction.x;
            if(Mathf.Abs(upDirection) + Mathf.Abs(rightDirection) < 0.01f)
            {
                animator.SetBool("Static", true);
                animator.SetBool("Up", false);
                animator.SetBool("Down", false);
                animator.SetBool("Right", false);
                animator.SetBool("Left", false);
            }
            else
            {
                if (Mathf.Abs(upDirection) > Mathf.Abs(rightDirection))
                {
                    if (upDirection > 0f)
                    {
                        animator.SetBool("Static", false);
                        animator.SetBool("Up", true);
                        animator.SetBool("Down", false);
                        animator.SetBool("Right", false);
                        animator.SetBool("Left", false);
                    }
                    else
                    {
                        animator.SetBool("Static", false);
                        animator.SetBool("Up", false);
                        animator.SetBool("Down", true);
                        animator.SetBool("Right", false);
                        animator.SetBool("Left", false);
                    }
                }
                else
                {
                    if (rightDirection > 0f)
                    {
                        animator.SetBool("Static", false);
                        animator.SetBool("Up", false);
                        animator.SetBool("Down", false);
                        animator.SetBool("Right", true);
                        animator.SetBool("Left", false);
                    }
                    else
                    {
                        animator.SetBool("Static", false);
                        animator.SetBool("Up", false);
                        animator.SetBool("Down", false);
                        animator.SetBool("Right", false);
                        animator.SetBool("Left", true);
                    }
                }
            }
        }
        Move();
    }
}
