﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Cette classe permet de répartir des rôles aux joueurs, que ce soit au démarrage du jeu ou à son redémarrage.
public class PlayerRoleManager : MonoBehaviour
{
    public enum Roles
    {
        Rebel = 0,
        Capitalist = 1,
        NotAffected = 2 // Ce rôle correspond au cas où un joueur n'existe pas
    }

    [SerializeField] Roles[] playerRoles;

    public Roles GetPlayerRole(int playerIndex)
    {
        if (playerIndex < playerRoles.Length)
        {
            return playerRoles[playerIndex];
        }
        else
        {
            throw new UnityException("playerIndex superior to "+ playerRoles.Length.ToString());
        }
    }

    public int playerNumber
    {
        get
        {
            int n = 0;
            foreach(Roles r in playerRoles)
            {
                if(r == Roles.Rebel || r == Roles.Capitalist)
                {
                    n++;
                }
            }
            return n;
        }
    }

    public static PlayerRoleManager instance;

	// Use this for initialization
	void Start ()
    {
		if(instance)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
            playerRoles = new Roles[4];
            for(int i = 0; i < 4; i++)
            {
                playerRoles[i] = Roles.NotAffected;
            }
        }
	}
	
    // A appeler pour affecter un rôle à un personnage en particulier. Deux personnages ne peuvent pas devenir boss
	public void AffectRole(int player, Roles role)
    {
        if(role == Roles.Capitalist)
        {
            MultiControllerHandler.Instance.CapitalistIndex = player;
            for(int i = 0; i < 4; i++)
            {
                if(playerRoles[i] != Roles.NotAffected)
                {
                    playerRoles[i] = Roles.Rebel;
                }
            }
        }
        playerRoles[player] = role;
    }

    public void RelaunchRoles(int capitalistPlayerIndex)
    {
        for(int i = 0; i < 4; i++)
        {
            if(i == capitalistPlayerIndex)
            {
                if(playerRoles[i] == Roles.NotAffected)
                {
                    throw new System.Exception("Not existing player can't be the boss !");
                }
                else
                {
                    playerRoles[i] = Roles.Capitalist;
                }
            }
            else if(playerRoles[i] != Roles.NotAffected)
            {
                playerRoles[i] = Roles.Rebel;
            }
        }
    }

}
