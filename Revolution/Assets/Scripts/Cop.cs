﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cop : Character {

    [SerializeField] private static bool alertmode;

    public static void setAlertMode(bool mode)
    {
        alertmode = mode;
    }
	int index = -1;
	public bool isActiveCop;
	public bool isBusy;

	public Vector3 target;
	Path path;
	int pathIndex = 0;
	public bool displayPath;
    

	// Use this for initialization
	override protected void Start ()
	{
		base.Start ();
		if (FindObjectOfType<Boss> () != null) {
			index = FindObjectOfType<Boss> ().AddCop (this);
		}
	}
	
	protected override void Update ()
	{
        if (alertmode)
        {
            speed = 0.0f;
        }
        
        else
        {
            if (index == -1)
            {
                if (Boss.instance != null)
                {
                    index = Boss.instance.AddCop(this);
                }
            }

            if (target.y >= 0 && path == null)
            {
                UpdatePath();
            }
            if (target.y >= 0)
            {
				if (((Mathf.Abs(transform.position.x - target.x) < 3f) && (Mathf.Abs(transform.position.z - target.z) < 3f)))
                {
                    target.y = -10;
                    path = null;
                    pathIndex = 0;
					direction = Vector3.zero;
					isBusy = false;
                }
            }
        }
		if (isActiveCop) {
			target.y = -10;
			isBusy = false;
		}
		base.Update ();
        if(BossController.instance.isSprinting)
        {
            rigidbody.velocity *= 3.0f;
        }


    }


	void OnDestroy(){
		if (FindObjectOfType<Boss> () != null) {
			FindObjectOfType<Boss> ().RemoveCop (index);
		}
	}

	protected override void SetDirection ()
	{
		if (isActiveCop) {
			direction = FindObjectOfType<Boss> ().direction;
		} else {
			if (path != null
				&& !((Mathf.Abs (transform.position.x - target.x) < 1.5f) && (Mathf.Abs (transform.position.z - target.z) < 1.5f))) {
				if (pathIndex < path.lookPoints.Length - 1 && ((Mathf.Abs (transform.position.x - path.lookPoints [pathIndex].x) < 1.5f) && (Mathf.Abs (transform.position.z - path.lookPoints [pathIndex].z) < 1.5f))) {
					pathIndex++;
				}
				direction = path.lookPoints [pathIndex] - transform.position;
				direction.y = 0;
				direction += new Vector3 (Random.Range (-1f, 1f), 0, Random.Range (-1f, 1f));
				direction = direction.normalized;
			}
		}
	}

	public void ReduceIndex(){
		index --;
	}

	void OnDrawGizmos(){
		if (isActiveCop) {
			Gizmos.color = Color.cyan;
			Gizmos.DrawSphere (transform.position + Vector3.up, 0.25f);
		}

		if (displayPath && path != null) {
			path.DrawWithGizmos ();
		}
	}

	public void OnPathFound(Vector3[] waypoints, bool pathSuccessful) {
		if (pathSuccessful) {
			path = new Path(waypoints, transform.position, 0, 0);
			isBusy = true;
		}
	}

	public void UpdatePath() {
		PathRequestManager.RequestPath (transform.position, target, OnPathFound);
	}

	public void SetActiveCop(){
		isActiveCop = true;
	}

	public void SetInactiveCop(){
		isActiveCop = false;
        direction = new Vector3(0, 0, 0);
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.GetComponent<RiotHeart>())
        {
			other.gameObject.GetComponent<RiotHeart> ().DestroyRiot();
		}
        else if(other.gameObject.tag=="Player")
        {
            other.gameObject.GetComponentInParent<Commoner>().Stun();
        }
	}

}
